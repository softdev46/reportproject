/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.albumproject.dao;

import com.peeraya.albumproject.helper.DatabaseHelper;
import com.peeraya.albumproject.model.ReportSale;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ADMIN
 */
public class SaleDao {

    public List<ReportSale> getDayReport() {
        ArrayList<ReportSale> list = new ArrayList();
        String sql = "SELECT strftime(\"%Y-%m-%d\", InvoiceDate) as period, SUM(Total) as total FROM invoices\n"
                + "LEFT JOIN invoice_items\n"
                + "ON invoices.InvoiceId = invoice_items.InvoiceId\n"
                + "GROUP BY period\n"
                + "ORDER BY period DESC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReportSale prod = ReportSale.fromRS(rs);
                list.add(prod);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<ReportSale> getMonthReport() {
        ArrayList<ReportSale> list = new ArrayList();
        String sql = "SELECT strftime(\"%Y-%m\", InvoiceDate) as period, SUM(Total) as total FROM invoices\n"
                + "LEFT JOIN invoice_items\n"
                + "ON invoices.InvoiceId = invoice_items.InvoiceId\n"
                + "GROUP BY period\n"
                + "ORDER BY period DESC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReportSale prod = ReportSale.fromRS(rs);
                list.add(prod);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<ReportSale> getYearReport(int year) {
        ArrayList<ReportSale> list = new ArrayList();
        String sql = "SELECT strftime(\"%Y-%m\", InvoiceDate) as period, SUM(Total) as total FROM invoices\n"
                + "LEFT JOIN invoice_items\n"
                + "ON invoices.InvoiceId = invoice_items.InvoiceId\n"
                + "WHERE strftime(\"%Y\", InvoiceDate)=\"" + year + "\"\n"
                + "GROUP BY period\n"
                + "ORDER BY period DESC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReportSale prod = ReportSale.fromRS(rs);
                list.add(prod);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
}
